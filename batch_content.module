<?php
/**
 * @file
 * Publish Content module file.
 */

/**
 * Implements hook_menu().
 */
function batch_content_menu() {
  $items['admin/config/content/publish'] = array(
    'title' => 'Publish content type',
    'description' => 'Publish all nodes from a content type',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('batch_content_get_settings_form'),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_LOCAL_TASK,
  );
  $items['admin/config/content/delete'] = array(
    'title' => 'Remove content',
    'description' => 'Remove content based on content type and other settings.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('batch_content_delete_content_form'),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_LOCAL_TASK,
  );
  $items['admin/config/content/orphan'] = array(
    'title' => 'Remove orphan content',
    'description' => 'Remove content based on content type and other settings.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('batch_content_delete_orphan_form'),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_LOCAL_TASK,
  );

  return $items;
}

/**
 * Page callback: Displays a form to select wich types to delete.
 *
 * @see batch_content_delete_content_form_validate()
 * @see batch_content_delete_content_form_submit()
 *
 * @ingroup forms
 */
function batch_content_delete_content_form($form, &$form_state) {
  $form['content_type'] = array(
    '#type' => 'checkboxes',
    '#title' => 'Content type',
    '#options' => _batch_content_options(),
    '#required' => TRUE,
  );
  $form['regex_search'] = array(
    '#type' => 'textfield',
    '#title' => t('Title containing'),
    '#description' => t('Use regex to search for a specific title.'),
    '#required' => TRUE,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Delete'),
  );
  return $form;
}

/**
 * Form validation handler for batch_content_delete_content_form().
 *
 * @see batch_content_delete_content_form_submit()
 */
function batch_content_delete_content_form_validate($form, &$form_state) {

}

/**
 * Form submission handler for batch_content_delete_content_form().
 *
 * @see batch_content_delete_content_form_validate()
 */
function batch_content_delete_content_form_submit($form, &$form_state) {

  $values = $form_state['values'];

  $types = array();
  foreach ($values['content_type'] as $type) {
    if ($type != '0') {
      $types[] = $type;
    }
  }

  $options['regex'] = $values['regex_search'];

  $batch = array(
    'title' => t('Deleting content type(s)'),
    'operations' => array(
      array('batch_content_delete_content', array($types, $options)),
    ),
    'finished' => 'batch_content_batch_finished',
  );

  batch_set($batch);
}

/**
 * Page callback: Displays a form to select wich types to delete.
 *
 * @see batch_content_delete_content_form_validate()
 * @see batch_content_delete_content_form_submit()
 *
 * @ingroup forms
 */
function batch_content_delete_orphan_form($form, &$form_state) {
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Delete'),
  );
  return $form;
}

/**
 * Form validation handler for batch_content_delete_content_form().
 *
 * @see batch_content_delete_content_form_submit()
 */
function batch_content_delete_orphan_form_validate($form, &$form_state) {

}

/**
 * Form submission handler for batch_content_delete_content_form().
 *
 * @see batch_content_delete_content_form_validate()
 */
function batch_content_delete_orphan_form_submit($form, &$form_state) {
  $values = $form_state['values'];

  $fields = db_select('field_config', 'f')
    ->fields('f', array('field_name'))
    ->condition('type', 'node_reference')
    ->execute()->fetchCol();

  $operations = array();
  foreach ($fields as $field) {
    $operations[] = array('batch_content_orphan', array($field, 'field_data_'));
    $operations[] = array(
      'batch_content_orphan',
      array($field, 'field_revision_'),
    );
  }

  $batch = array(
    'title' => t('Deleting content type(s)'),
    'operations' => $operations,
    'finished' => 'batch_content_batch_finished',
  );

  batch_set($batch);
}

/**
 * Batch callback that process orphaned content.
 *
 * @param string $field
 * @param string $table
 * @param array $context
 */
function batch_content_orphan($field, $table, &$context) {
  // Create the current table name.
  $current = $table . $field;

  if (empty($context['sandbox'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['current_node'] = 0;

    $count = db_select($current, 'f')
      ->fields('f')
      ->condition(db_or()->isNull($field . '_nid')->isNull('n.nid'));
    $count->leftJoin('node', 'n', $field . '_nid = n.nid');
    $count = $count->countQuery()->execute()->fetchField();

    $context['sandbox']['max'] = $count;
  }

  // Delete this many at a batch.
  $limit = 40;

  $rids = db_select($current, 'c')
    ->fields('c', array('revision_id'))
    ->condition(db_or()->isNull($field . '_nid')->isNull('n.nid'))
    ->range(0, $limit);
  $rids->leftJoin('node', 'n', $field . '_nid = n.nid');
  $rids = $rids->execute()->fetchCol();

  if (!empty($rids)) {
    $delete = db_delete($current)
      ->condition('revision_id', $rids, 'IN')
      ->execute();
  }

  // $context['results'][] = $nid;
  $context['sandbox']['progress'] += $limit;
  // $context['sandbox']['current_node'] = $nid;
  // $context['message'] = check_plain('Deleted: ' . $nid);

  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }

  if ($context['sandbox']['max'] == 0) {
    $context['finished'] = 1;
  }
}

/**
 * Page callback drupal_get_form batch_content_get_settings_form.
 */
function batch_content_get_settings_form($form, &$form_state) {
  $form = array();

  $form['content_type'] = array(
    '#type' => 'checkboxes',
    '#title' => 'Content type',
    '#options' => _batch_content_options(),
    '#required' => TRUE,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Send'),
  );

  return $form;
}

/**
 * function _batch_content_options.
 *
 * @return array
 *   Containing content types.
 */
function _batch_content_options($id = NULL) {
  // Get all content types.
  $content_types = node_type_get_types();
  $arr = array();
  // Fill the array.
  foreach ($content_types as $type) {
    $arr[$type->type] = $type->name;
  }
  if ($id <= 0 && isset($arr[$id])) {
    return $arr[$id];
  }
  else {
    return $arr;
  }
}

/**
 * Callback form validate.
 */
function batch_content_get_settings_form_validate($form, &$form_state) {
  $values = $form_state['values'];

  // Check if field is not empty.
  $content_type = TRUE;
  foreach ($values['content_type'] as $type) {
    if ($type != '0') {
      $content_type = FALSE;
    }
  }
  if ($content_type) {
    form_set_error('content_type', t('Field cannot be empty.'));
  }
}

/**
 * Callback form submit.
 */
function batch_content_get_settings_form_submit($form, &$form_state) {
  $values = $form_state['values'];

  $types = array();
  foreach ($values['content_type'] as $type) {
    if ($type != '0') {
      $types[] = $type;
    }
  }

  $batch = array(
    'title' => t('Updating content type(s)'),
    'operations' => array(
      array('batch_content_update_content_types', array($types)),
    ),
    'finished' => 'batch_content_batch_finished',
  );

  batch_set($batch);
}

/**
 * Batch callbak: remove all nodes of type based on title.
 */
function batch_content_delete_content($types, $options, &$context) {
  if (empty($context['sandbox'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['current_node'] = 0;
    $count = db_select('{node}', 'n')
      ->condition('n.type', $types, 'IN')
      ->where('n.title REGEXP :regex', array(':regex' => $options['regex']))
      ->countQuery()->execute()->fetchField();
    $context['sandbox']['max'] = $count;
  }

  // Delete this many in a batch.
  $limit = 20;

  $nids = db_select('node', 'n')
    ->fields('n', array('nid'))
    ->condition('n.type', $types, 'IN')
    ->where('n.title REGEXP :regex', array(':regex' => $options['regex']))
    ->range(0, $limit)
    ->execute()->fetchCol();

  foreach ($nids as $nid) {
    node_delete($nid);

    $context['results'][] = $nid;
    $context['sandbox']['progress']++;
    $context['sandbox']['current_node'] = $nid;
    $context['message'] = check_plain('Deleted: ' . $nid);
  }

  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}

/**
 * Batch callback.
 */
function batch_content_update_content_types($types, &$context) {

  if (empty($context['sandbox'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['current_node'] = 0;
    $count = db_select('{node}', 'n')
      ->condition('n.type', $types, 'IN')
      ->countQuery()->execute()->fetchField();
    $context['sandbox']['max'] = $count;
  }

  $limit = 5;
  $result = db_select('{node}', 'n')
    ->fields('n', array('nid'))
    ->condition('n.type', $types, 'IN')
    ->condition('n.nid', $context['sandbox']['current_node'], '>')
    ->orderBy('n.nid')
    ->range(0, $limit)
    ->execute();


  foreach ($result as $row) {
    $node = node_load($row->nid);
    $node->published = TRUE;
    node_save($node);

    $context['results'][] = $node->nid . ' : ' . check_plain($node->title);
    $context['sandbox']['progress']++;
    $context['sandbox']['current_node'] = $node->nid;
    $context['message'] = check_plain($node->title);
  }
  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}

/**
 * Batch callback finished.
 */
function batch_content_batch_finished($success, $results, $operations) {
  if ($success) {
    $message = format_plural(count($results), 'One post processed', '@count posts processed');
  }
  else {
    $message = t('Finished with an error.');
  }
  drupal_set_message($message);
}
