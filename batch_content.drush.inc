<?php

/**
 * @file
 * Contains all the functions required for the drush commands.
 */

/**
 * Implements hook_drush_command().
 */
function batch_content_drush_command() {
  $items['clean-file-usage'] = array(
    'description' => 'Cleans the file_usage table. Removes all none existing links.',
    'aliases' => array('cfu'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_DATABASE,
  );
  return $items;
}

/**
 * Drush callback: Removes orphaned files from the file_managed table.
 */
function drush_batch_content_clean_file_usage() {
  // Query the file_usage table.
  $query = db_select('file_usage', 'f')
    ->fields('f', array());
  // Join to the file_managed table;
  $query->leftJoin('file_managed', 'fm', 'f.fid = fm.fid');
  $query->where('fm.fid is null');
  $results = $query->execute()->fetchAllKeyed();

  // A simpel count of deleted items.
  $deleted = 0;

  $fids = array_keys($results);
  // Remove the file_usage records per 50 to support sqlite.
  while ($ids = array_splice($fids, 0, 50)) {
    db_delete('file_usage')
      ->condition('fid', $ids, 'IN')
      ->execute();

    // Keep track of deleted items.
    $deleted += count($ids);
  }

  // Report how many items are deleted.
  print_r('Deleted ' . $deleted . ' items.' . PHP_EOL);
}
